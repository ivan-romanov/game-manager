package org.bitbucket.iromanov.gm.service;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import org.bitbucket.iromanov.gm.model.answer.Answer;
import org.bitbucket.iromanov.gm.model.answer.CorrectAnswer;
import org.bitbucket.iromanov.gm.model.answer.WrongAnswer;
import org.bitbucket.iromanov.gm.model.result.Result;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RequestHandlerServiceImplTest {

    @Mock
    private GamesSettingsService settings;
    @Mock
    private GamesStateManger state;

    @InjectMocks
    private RequestHandlerServiceImpl requestHandler;

    @Test
    @DisplayName("Increase player points if answer is correct")
    void increasePlayerPointsIfAnswerIsCorrect() {
        when(settings.verifyAnswer(1, 2, 3)).thenReturn(new CorrectAnswer(10));
        when(state.isPlayerExists(1, "Ivan")).thenReturn(true);

        requestHandler.handleAnswer(
            Answer.builder()
                .gameId(1L)
                .playerName("Ivan")
                .questionId(2)
                .optionId(3)
                .build());

        verify(state, times(1)).increasePlayerPoints(1, "Ivan", 10);
    }

    @Test
    @DisplayName("Doesn't increase player points if answer is wrong")
    void doesntIncreasePlayerPointsIfAnswerIsWrong() {
        when(settings.verifyAnswer(1, 2, 3)).thenReturn(new WrongAnswer());
        when(state.isPlayerExists(1, "Ivan")).thenReturn(true);

        requestHandler.handleAnswer(
            Answer.builder()
                .gameId(1L)
                .playerName("Ivan")
                .questionId(2)
                .optionId(3)
                .build());

        verify(state, never()).increasePlayerPoints(anyLong(), anyString(), anyInt());
    }

    @Test
    @DisplayName("Create player if doesn't exist")
    void createPlayerIfDoestExist() {
        when(settings.verifyAnswer(1, 2, 3)).thenReturn(new WrongAnswer());
        when(state.isPlayerExists(1, "Ivan")).thenReturn(false);

        requestHandler.handleAnswer(
            Answer.builder()
                .gameId(1L)
                .playerName("Ivan")
                .questionId(2)
                .optionId(3)
                .build());

        verify(state, times(1)).createGamePlayer(1, "Ivan");
    }

    @Test
    @DisplayName("Doesnt create new player ifexist")
    void doesntCreatePlayerIfDoestExist() {
        when(settings.verifyAnswer(1, 2, 3)).thenReturn(new WrongAnswer());
        when(state.isPlayerExists(1, "Ivan")).thenReturn(true);

        requestHandler.handleAnswer(
            Answer.builder()
                .gameId(1L)
                .playerName("Ivan")
                .questionId(2)
                .optionId(3)
                .build());

        verify(state, times(0)).createGamePlayer(anyInt(), anyString());
    }

    @Test
    @DisplayName("Returns scoreboard in desc order")
    void returnsScoreboardInDescOrder() {
        Result ivan = new Result("Ivan", 10);
        Result tal = new Result("Tal", 5);
        Result mira = new Result("Mira", 18);
        Result donald = new Result("Donald", 1);

        when(state.getGamePlayersResults(1)).thenReturn(Arrays.asList(ivan, tal, mira, donald));

        List<Result> actual = requestHandler.getScoreboard(1);

        assertThat(actual).containsExactly(mira, ivan, tal, donald);

    }
}
