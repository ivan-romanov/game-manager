package org.bitbucket.iromanov.gm.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

import org.bitbucket.iromanov.gm.TestData;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationResult;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationStatus;
import org.bitbucket.iromanov.gm.model.exception.GameSettingsNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InMemoryGamesSettingsServiceTest {

    @Mock
    private GamesSettingsProvider gameRepository;

    @InjectMocks
    private InMemoryGamesSettingsService gamesSettingsService;

    @BeforeEach
    void init() {
        when(gameRepository.getGameSettings(1)).thenReturn(TestData.testGame);
    }

    @Test
    @DisplayName("Verify answer returns correct answer with points if correct")
    public void verifyAnswerReturnsCorrectAnswerWithPointsIfCorrect() {
        AnswerVerificationResult actual = gamesSettingsService.verifyAnswer(1L, 1, 3);

        assertThat(actual.getPoints()).isEqualTo(5);
        assertThat(actual.getStatus()).isEqualTo(AnswerVerificationStatus.CORRECT);
    }

    @Test
    @DisplayName("Verify answer returns wrong answer with 0 if wrong")
    public void verifyAnswerReturnsWrongAnswerWith0IfWrong() {
        AnswerVerificationResult actual = gamesSettingsService.verifyAnswer(1L, 1, 2);

        assertThat(actual.getPoints()).isEqualTo(0);
        assertThat(actual.getStatus()).isEqualTo(AnswerVerificationStatus.WRONG);
    }

    @Test
    @DisplayName("Verify answer throws GameSettingsNotFoundException if question not found")
    public void verifyAnswerThrowsGameSettingsNotFoundExceptionIfQuestionNotFound() {
        assertThatThrownBy(() -> gamesSettingsService.verifyAnswer(1L, 4, 1))
            .isInstanceOf(GameSettingsNotFoundException.class);
    }

    @Test
    @DisplayName("Verify answer throws GameSettingsNotFoundException if option not found")
    public void verifyAnswerThrowsGameSettingsNotFoundExceptionIfOptionNotFound() {
        assertThatThrownBy(() -> gamesSettingsService.verifyAnswer(1L, 1, 4))
            .isInstanceOf(GameSettingsNotFoundException.class);
    }

}
