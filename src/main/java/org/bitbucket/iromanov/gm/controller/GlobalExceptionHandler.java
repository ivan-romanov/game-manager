package org.bitbucket.iromanov.gm.controller;


import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.iromanov.gm.controller.dto.ErrorDto;
import org.bitbucket.iromanov.gm.model.exception.GameManagerInternalException;
import org.bitbucket.iromanov.gm.model.exception.GameSettingsNotFoundException;
import org.bitbucket.iromanov.gm.model.exception.GameStateBadRequestException;
import org.bitbucket.iromanov.gm.model.exception.GameStateNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleServerException(HttpServletRequest req, Exception e) {
        log.error("Internal server error", e);
        return new ResponseEntity<>(new ErrorDto("Internal server error"), new HttpHeaders(),
            HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(GameManagerInternalException.class)
    public ResponseEntity<Object> handleGameStateInternalException(HttpServletRequest req, Exception e) {
        log.error("Game manager exception", e);
        return new ResponseEntity<>(new ErrorDto(e.getMessage()), new HttpHeaders(),
            HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(GameSettingsNotFoundException.class)
    public ResponseEntity<Object> handleGameSettingsNotFoundException(HttpServletRequest req, Exception e) {
        log.error("Game settings not found exception", e);
        return new ResponseEntity<>(new ErrorDto(e.getMessage()), new HttpHeaders(),
            HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GameStateNotFoundException.class)
    public ResponseEntity<Object> handleGameStateNotFoundException(HttpServletRequest req, Exception e) {
        log.error("Game state not found exception", e);
        return new ResponseEntity<>(new ErrorDto(e.getMessage()), new HttpHeaders(),
            HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GameStateBadRequestException.class)
    public ResponseEntity<Object> handleGameStateBadRequestExceptionException(HttpServletRequest req, Exception e) {
        log.error("Game state not found exception", e);
        return new ResponseEntity<>(new ErrorDto(e.getMessage()), new HttpHeaders(),
            HttpStatus.BAD_REQUEST);
    }

}
