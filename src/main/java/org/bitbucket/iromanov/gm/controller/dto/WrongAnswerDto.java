package org.bitbucket.iromanov.gm.controller.dto;

import lombok.AllArgsConstructor;
import org.springframework.lang.NonNull;

@AllArgsConstructor
public class WrongAnswerDto implements AnswerVerificationResultDto {

    @Override
    @NonNull
    public AnswerStatusDto getStatus() {
        return AnswerStatusDto.WRONG;
    }

    @Override
    public int getPoints() {
        return 0;
    }
}
