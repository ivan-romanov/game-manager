package org.bitbucket.iromanov.gm.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.NonNull;

@AllArgsConstructor
@Getter
public class PlayerScoreDto {

    @NonNull
    private final String name;
    private final int points;
}
