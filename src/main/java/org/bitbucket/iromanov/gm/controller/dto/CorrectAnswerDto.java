package org.bitbucket.iromanov.gm.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.NonNull;

@Getter
@AllArgsConstructor
public class CorrectAnswerDto implements AnswerVerificationResultDto {

    private final int points;

    @Override
    @NonNull
    public AnswerStatusDto getStatus() {
        return AnswerStatusDto.CORRECT;
    }
}
