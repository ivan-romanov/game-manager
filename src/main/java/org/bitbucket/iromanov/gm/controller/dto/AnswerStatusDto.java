package org.bitbucket.iromanov.gm.controller.dto;


public enum AnswerStatusDto {
    WRONG,
    CORRECT
}
