package org.bitbucket.iromanov.gm.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Getter
@Setter
@AllArgsConstructor
public class ErrorDto {

    @Nullable
    private String message;
}
