package org.bitbucket.iromanov.gm.controller;

import static org.bitbucket.iromanov.gm.model.answer.AnswerVerificationStatus.CORRECT;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.bitbucket.iromanov.gm.controller.dto.AnswerVerificationResultDto;
import org.bitbucket.iromanov.gm.controller.dto.CorrectAnswerDto;
import org.bitbucket.iromanov.gm.controller.dto.ErrorDto;
import org.bitbucket.iromanov.gm.controller.dto.PlayerScoreDto;
import org.bitbucket.iromanov.gm.controller.dto.WrongAnswerDto;
import org.bitbucket.iromanov.gm.model.answer.Answer;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationResult;
import org.bitbucket.iromanov.gm.service.RequestHandlerService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@AllArgsConstructor
@RestController
@RequestMapping(value = "/game-manager", produces = MediaType.APPLICATION_JSON_VALUE)
public class GameController {

    private final RequestHandlerService requestHandler;

    @ApiOperation("Get given game scoreboard")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = PlayerScoreDto.class, responseContainer = "List"),
        @ApiResponse(code = 404, message = "Game not found", response = ErrorDto.class),
        @ApiResponse(code = 500, message = "Internal Error", response = ErrorDto.class)
    })
    @GetMapping("/games/{gameId}/players/")
    public List<PlayerScoreDto> getScoreboard(@PathVariable long gameId) {
        return requestHandler.getScoreboard(gameId)
            .stream()
            .map(result -> new PlayerScoreDto(result.getPlayerName(), result.getPoints()))
            .collect(Collectors.toList());
    }

    @ApiOperation("Submit question answer")
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = AnswerVerificationResultDto.class),
        @ApiResponse(code = 400, message = "Bad request", response = ErrorDto.class),
        @ApiResponse(code = 404, message = "Not found", response = ErrorDto.class),
        @ApiResponse(code = 500, message = "Internal Error", response = ErrorDto.class)
    })
    @PutMapping("/games/{gameId}/players/{playerName}/questions/{questionId}/answer/{optionId}")
    public AnswerVerificationResultDto submitAnswer(
        @PathVariable int gameId,
        @PathVariable String playerName,
        @PathVariable int questionId,
        @PathVariable int optionId
    ) {
        Answer answer = Answer.builder()
            .gameId(gameId)
            .playerName(playerName)
            .questionId(questionId)
            .optionId(optionId)
            .build();

        AnswerVerificationResult answerVerificationResult = requestHandler.handleAnswer(answer);

        if (answerVerificationResult.getStatus() == CORRECT) {
            return new CorrectAnswerDto(answerVerificationResult.getPoints());
        } else {
            return new WrongAnswerDto();
        }
    }

}
