package org.bitbucket.iromanov.gm.controller.dto;

import org.springframework.lang.NonNull;

public interface AnswerVerificationResultDto {

    @NonNull
    AnswerStatusDto getStatus();

    int getPoints();
}
