package org.bitbucket.iromanov.gm.service;

import java.util.List;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationResult;
import org.bitbucket.iromanov.gm.model.game.Game;

/**
 * Handles predefined games settings
 */
public interface GamesSettingsService {

    /**
     * Returns all predefined games
     */
    List<Game> getAllGames();

    /**
     * Verify answer correctness
     */
    AnswerVerificationResult verifyAnswer(long gameId, int questionId, int optionId);
}
