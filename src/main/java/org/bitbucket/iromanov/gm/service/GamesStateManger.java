package org.bitbucket.iromanov.gm.service;


import java.util.List;
import org.bitbucket.iromanov.gm.model.result.Result;
import org.springframework.lang.NonNull;

/**
 * Manages Games State
 */
public interface GamesStateManger {

    List<Result> getGamePlayersResults(long gameId);

    boolean isPlayerExists(long gameId, @NonNull String playerName);

    void createGamePlayer(long gameId, @NonNull String playerName);

    void submitAnswer(long gameId, @NonNull String playerName, int questionId, int optionId);

    int increasePlayerPoints(long gameId, @NonNull String playerName, int points);

}
