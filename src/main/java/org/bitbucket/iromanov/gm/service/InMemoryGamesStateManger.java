package org.bitbucket.iromanov.gm.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.bitbucket.iromanov.gm.model.exception.GameManagerInternalException;
import org.bitbucket.iromanov.gm.model.exception.GameStateNotFoundException;
import org.bitbucket.iromanov.gm.model.result.Result;
import org.bitbucket.iromanov.gm.model.state.GameState;
import org.bitbucket.iromanov.gm.model.state.PlayerState;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InMemoryGamesStateManger implements GamesStateManger {

    private Map<Long, GameState> games;
    private final GamesSettingsService gamesSettingsService;

    @Override
    public List<Result> getGamePlayersResults(long gameId) {
        return getGameState(gameId).getPlayerStates()
            .stream()
            .map(playerState -> new Result(playerState.getPlayerName(), playerState.getTotalPoints()))
            .collect(Collectors.toList());
    }

    @Override
    public boolean isPlayerExists(long gameId, @NonNull String playerName) {
        return getGameState(gameId).findPlayer(playerName).isPresent();
    }

    @Override
    public void createGamePlayer(long gameId, @NonNull String playerName) {
        getGameState(gameId).createPlayer(playerName);
    }

    @Override
    public void submitAnswer(long gameId, @NonNull String playerName, int questionId, int optionId) {
        getPlayerResults(gameId, playerName).addAnswer(questionId, optionId);
    }

    @Override
    public int increasePlayerPoints(long gameId, @NonNull String playerName, int points) {
        return getPlayerResults(gameId, playerName).increasePoints(points);
    }

    @NonNull
    private GameState getGameState(long gameId) {
        GameState gameState = games.get(gameId);
        if (gameState == null) {
            throw new GameStateNotFoundException("Game doesn't exist, gameId =" + gameId);
        }
        return gameState;
    }

    private PlayerState getPlayerResults(long gameId, @NonNull String playerName) {
        return getGameState(gameId)
            .findPlayer(playerName)
            .orElseThrow(() -> new GameManagerInternalException("Player doesn't exists, playerName =" + playerName));
    }

    @PostConstruct
    private void initEmptyGames() {
        games = gamesSettingsService.getAllGames()
            .stream()
            .map(game -> GameState.builder()
                .gameId(game.getId())
                .playerStates(new ArrayList<>())
                .build())
            .collect(Collectors.toMap(GameState::getGameId, gameState -> gameState));
    }

}
