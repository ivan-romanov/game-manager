package org.bitbucket.iromanov.gm.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.bitbucket.iromanov.gm.model.exception.GameSettingsNotFoundException;
import org.bitbucket.iromanov.gm.model.game.Game;
import org.bitbucket.iromanov.gm.support.HardcodedGames;
import org.springframework.stereotype.Component;

@Component
public class HardcodedGamesSettingsProvider implements GamesSettingsProvider {

    private Map<Long, Game> games;

    @Override
    public Game getGameSettings(long gameId) {
        Game game = games.get(gameId);

        if (game == null) {
            throw new GameSettingsNotFoundException("Game doesn't exist, gameId = " + gameId);
        }
        return game;
    }

    @Override
    public List<Game> getAllGamesSettings() {
        return new ArrayList<>(games.values());
    }

    /**
     * Just for demo app. In real-life application game settings will be provided through persistence layer or
     * configuration file. Assume that configuration is correct and doesn't check it.
     */
    @PostConstruct
    private void initSettings() {
        games = HardcodedGames.get().stream()
            .collect(Collectors.toMap(Game::getId, game -> game));
    }
}
