package org.bitbucket.iromanov.gm.service;

import java.util.List;
import org.bitbucket.iromanov.gm.model.answer.Answer;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationResult;
import org.bitbucket.iromanov.gm.model.result.Result;

public interface RequestHandlerService {

    List<Result> getScoreboard(long gameId);

    AnswerVerificationResult handleAnswer(Answer answer);
}
