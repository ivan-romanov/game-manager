package org.bitbucket.iromanov.gm.service;

import java.util.List;
import org.bitbucket.iromanov.gm.model.game.Game;

public interface GamesSettingsProvider {

    /**
     * Get single game settings
     */
    Game getGameSettings(long gameId);

    /**
     * Get all games settings
     */
    List<Game> getAllGamesSettings();
}
