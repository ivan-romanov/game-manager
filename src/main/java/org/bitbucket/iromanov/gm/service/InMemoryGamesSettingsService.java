package org.bitbucket.iromanov.gm.service;

import java.util.List;
import lombok.AllArgsConstructor;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationResult;
import org.bitbucket.iromanov.gm.model.answer.CorrectAnswer;
import org.bitbucket.iromanov.gm.model.answer.WrongAnswer;
import org.bitbucket.iromanov.gm.model.game.AnswerOption;
import org.bitbucket.iromanov.gm.model.game.Game;
import org.bitbucket.iromanov.gm.model.game.Question;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class InMemoryGamesSettingsService implements GamesSettingsService {

    private final GamesSettingsProvider gameRepository;

    @Override
    public List<Game> getAllGames() {
        return gameRepository.getAllGamesSettings();
    }

    @Override
    public AnswerVerificationResult verifyAnswer(long gameId, int questionId, int optionId) {
        Game game = gameRepository.getGameSettings(gameId);
        Question question = game.getQuestionById(questionId);
        AnswerOption option = question.getAnswerOptionById(optionId);

        if (isCorrectAnswer(question, option.getId())) {
            return new CorrectAnswer(question.getPoints());
        } else {
            return new WrongAnswer();
        }
    }

    private boolean isCorrectAnswer(Question question, int optionId) {
        return optionId == question.getCorrectAnswerId();
    }
}
