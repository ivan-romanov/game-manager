package org.bitbucket.iromanov.gm.service;

import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.bitbucket.iromanov.gm.model.answer.Answer;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationResult;
import org.bitbucket.iromanov.gm.model.answer.AnswerVerificationStatus;
import org.bitbucket.iromanov.gm.model.result.Result;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RequestHandlerServiceImpl implements RequestHandlerService {

    private final GamesSettingsService settings;
    private final GamesStateManger gameState;

    @Override
    public List<Result> getScoreboard(long gameId) {
        return gameState.getGamePlayersResults(gameId)
            .stream()
            .sorted((result1, result2) -> Integer.compare(result2.getPoints(), result1.getPoints()))
            .collect(Collectors.toList());
    }

    public AnswerVerificationResult handleAnswer(Answer answer) {
        final long gameId = answer.getGameId();
        final String playerName = answer.getPlayerName();
        final int questionId = answer.getQuestionId();
        final int optionId = answer.getOptionId();

        AnswerVerificationResult answerVerificationResult =
            verifyAnswerCorrectness(gameId, questionId, optionId);

        if (playerDoesntExist(gameId, playerName)) {
            createNewPlayer(gameId, playerName);
        }

        submitAnswer(gameId, playerName, questionId, optionId);

        if (chosenAnswerIsCorrect(answerVerificationResult)) {
            increasePlayerPoints(gameId, playerName, answerVerificationResult);
        }

        return answerVerificationResult;
    }

    private AnswerVerificationResult verifyAnswerCorrectness(long gameId, int questionId, int optionId) {
        return settings.verifyAnswer(gameId, questionId, optionId);
    }

    private boolean playerDoesntExist(long gameId, String playerName) {
        return !gameState.isPlayerExists(gameId, playerName);
    }

    private void createNewPlayer(long gameId, String playerName) {
        gameState.createGamePlayer(gameId, playerName);
    }

    private void submitAnswer(long gameId, String playerName, int questionId, int optionId) {
        gameState.submitAnswer(gameId, playerName, questionId, optionId);
    }

    private boolean chosenAnswerIsCorrect(AnswerVerificationResult answerVerificationResult) {
        return answerVerificationResult.getStatus() == AnswerVerificationStatus.CORRECT;
    }

    private void increasePlayerPoints(long gameId, String playerName,
        AnswerVerificationResult answerVerificationResult) {
        gameState.increasePlayerPoints(gameId, playerName, answerVerificationResult.getPoints());
    }

}
