package org.bitbucket.iromanov.gm.support;


import java.util.Arrays;
import java.util.List;
import org.bitbucket.iromanov.gm.model.game.AnswerOption;
import org.bitbucket.iromanov.gm.model.game.Game;
import org.bitbucket.iromanov.gm.model.game.Question;

public class HardcodedGames {

    final private static Game animalsGame = Game.builder()
        .id(1L)
        .title("Animals")
        .questions(Arrays.asList(
            Question.builder()
                .id(1)
                .text("The Asian Elephants trunk contains up to how many muscles?")
                .answerOptions(Arrays.asList(
                    AnswerOption.builder()
                        .id(1)
                        .text("600")
                        .build(),
                    AnswerOption.builder()
                        .id(2)
                        .text("6000")
                        .build(),
                    AnswerOption.builder()
                        .id(3)
                        .text("6000")
                        .build()
                    )
                )
                .correctAnswerId(3)
                .points(5)
                .build(),
            Question.builder()
                .id(2)
                .text("The Bichon Frise is a breed of what?")
                .answerOptions(Arrays.asList(
                    AnswerOption.builder()
                        .id(1)
                        .text("Dog")
                        .build(),
                    AnswerOption.builder()
                        .id(2)
                        .text("Cow")
                        .build(),
                    AnswerOption.builder()
                        .id(3)
                        .text("Sheep")
                        .build()
                    )
                )
                .correctAnswerId(1)
                .points(10)
                .build(),
            Question.builder()
                .id(3)
                .text("What is the top speed of a Bottle Nosed Dolphin?")
                .answerOptions(Arrays.asList(
                    AnswerOption.builder()
                        .id(1)
                        .text("21mph")
                        .build(),
                    AnswerOption.builder()
                        .id(2)
                        .text("26mph")
                        .build(),
                    AnswerOption.builder()
                        .id(3)
                        .text("31mph")
                        .build()
                    )
                )
                .correctAnswerId(1)
                .points(8)
                .build()
        ))
        .build();

    final private static Game itGame = Game.builder()
        .id(2L)
        .title("IT")
        .questions(Arrays.asList(
            Question.builder()
                .id(1)
                .text("Number of bit used by the IPv6 address")
                .answerOptions(Arrays.asList(
                    AnswerOption.builder()
                        .id(1)
                        .text("32 bit")
                        .build(),
                    AnswerOption.builder()
                        .id(2)
                        .text("64 bit")
                        .build(),
                    AnswerOption.builder()
                        .id(3)
                        .text("128 bit")
                        .build(),
                    AnswerOption.builder()
                        .id(4)
                        .text("256 bit")
                        .build()
                    )
                )
                .correctAnswerId(3)
                .points(15)
                .build(),
            Question.builder()
                .id(2)
                .text("Which of the following is not an operating system")
                .answerOptions(Arrays.asList(
                    AnswerOption.builder()
                        .id(1)
                        .text("DOS")
                        .build(),
                    AnswerOption.builder()
                        .id(2)
                        .text("C")
                        .build(),
                    AnswerOption.builder()
                        .id(3)
                        .text("Mac")
                        .build(),
                    AnswerOption.builder()
                        .id(4)
                        .text("Linux")
                        .build()
                    )
                )
                .correctAnswerId(2)
                .points(5)
                .build()
        ))
        .build();

    public static List<Game> get() {
        return Arrays.asList(animalsGame, itGame);
    }
}
