package org.bitbucket.iromanov.gm.model.answer;

import org.springframework.lang.NonNull;

public interface AnswerVerificationResult {

    @NonNull
    AnswerVerificationStatus getStatus();

    int getPoints();
}
