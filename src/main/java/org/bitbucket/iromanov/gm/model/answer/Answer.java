package org.bitbucket.iromanov.gm.model.answer;

import lombok.Builder;
import lombok.Getter;
import org.springframework.lang.NonNull;

@Builder
@Getter
public class Answer {

    private final long gameId;
    @NonNull
    private final String playerName;
    private final int questionId;
    private final int optionId;
}
