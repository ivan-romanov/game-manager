package org.bitbucket.iromanov.gm.model.result;


import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.NonNull;

@AllArgsConstructor
@Getter
public class Result {
    @NonNull
    private final String playerName;
    private final int points;
}
