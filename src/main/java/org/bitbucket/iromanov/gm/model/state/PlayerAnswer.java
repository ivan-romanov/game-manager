package org.bitbucket.iromanov.gm.model.state;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PlayerAnswer {

    private final int questionId;
    private final int givenAnswer;
}
