package org.bitbucket.iromanov.gm.model.game;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.iromanov.gm.model.exception.GameSettingsNotFoundException;
import org.springframework.lang.NonNull;

@Getter
@Builder
public class Game {

    private final long id;
    @NonNull
    private final String title;
    @NonNull
    private final List<Question> questions;

    public Question getQuestionById(int questionId) {
        return questions
            .stream()
            .filter(question -> question.getId() == questionId)
            .findFirst()
            .orElseThrow(() -> new GameSettingsNotFoundException("Question doesn't exist, questionId = " + questionId));
    }
}
