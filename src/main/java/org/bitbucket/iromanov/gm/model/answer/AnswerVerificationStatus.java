package org.bitbucket.iromanov.gm.model.answer;


public enum AnswerVerificationStatus {
    WRONG,
    CORRECT
}
