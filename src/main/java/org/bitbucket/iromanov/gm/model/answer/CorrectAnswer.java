package org.bitbucket.iromanov.gm.model.answer;

import static org.bitbucket.iromanov.gm.model.answer.AnswerVerificationStatus.CORRECT;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.lang.NonNull;

@Getter
@AllArgsConstructor
public class CorrectAnswer implements AnswerVerificationResult {

    private final int points;

    @Override
    @NonNull
    public AnswerVerificationStatus getStatus() {
        return CORRECT;
    }

    @Override
    public int getPoints() {
        return points;
    }
}
