package org.bitbucket.iromanov.gm.model.exception;

public class GameStateNotFoundException extends RuntimeException {

    public GameStateNotFoundException(String message) {
        super(message);
    }
}
