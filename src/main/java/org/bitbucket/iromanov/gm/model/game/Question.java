package org.bitbucket.iromanov.gm.model.game;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.iromanov.gm.model.exception.GameSettingsNotFoundException;
import org.springframework.lang.NonNull;

@Getter
@Builder
public class Question {

    private final int id;
    private final int points;
    private final int correctAnswerId;
    @NonNull
    private final String text;
    @NonNull
    private final List<AnswerOption> answerOptions;

    public AnswerOption getAnswerOptionById(int optionId) {
        return answerOptions
            .stream()
            .filter(option -> option.getId() == optionId)
            .findFirst()
            .orElseThrow(() -> new GameSettingsNotFoundException("Option doesn't exist, optionId =" + optionId));
    }


}
