package org.bitbucket.iromanov.gm.model.game;

import lombok.Builder;
import lombok.Getter;
import org.springframework.lang.NonNull;

@Getter
@Builder
public class AnswerOption {

    private final int id;
    @NonNull
    private final String text;
}
