package org.bitbucket.iromanov.gm.model.exception;

public class GameStateBadRequestException extends RuntimeException{

    public GameStateBadRequestException(String message) {
        super(message);
    }
}
