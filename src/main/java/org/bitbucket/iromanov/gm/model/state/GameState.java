package org.bitbucket.iromanov.gm.model.state;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.iromanov.gm.model.exception.GameManagerInternalException;
import org.springframework.lang.NonNull;

@Builder
@Getter
public class GameState {

    private final long gameId;
    @NonNull
    private final List<PlayerState> playerStates;

    public Optional<PlayerState> findPlayer(@NonNull String playerName) {
        return playerStates
            .stream()
            .filter(playerResults -> playerName.equals(playerResults.getPlayerName()))
            .findFirst();
    }

    public void createPlayer(@NonNull String playerName) {
        if (findPlayer(playerName).isPresent()) {
            throw new GameManagerInternalException("Try to create player already exists");
        }

        PlayerState results = PlayerState.builder()
            .playerName(playerName)
            .answers(new ArrayList<>())
            .build();

        playerStates.add(results);
    }
}
