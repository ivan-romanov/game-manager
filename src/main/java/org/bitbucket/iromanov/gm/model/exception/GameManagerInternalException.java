package org.bitbucket.iromanov.gm.model.exception;

public class GameManagerInternalException extends RuntimeException {

    public GameManagerInternalException(String message) {
        super(message);
    }
}
