package org.bitbucket.iromanov.gm.model.state;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.iromanov.gm.model.exception.GameStateBadRequestException;
import org.springframework.lang.NonNull;

@Getter
@Builder
public class PlayerState {

    @NonNull
    private final String playerName;
    private int totalPoints;
    @NonNull
    private final List<PlayerAnswer> answers;

    public int increasePoints(int points) {
        totalPoints += points;
        return totalPoints;
    }

    public void addAnswer(int questionId, int optionId) {
        if (answerExist(questionId)) {
            throw new GameStateBadRequestException("Answer already exists, questionId = " + questionId);
        }

        answers.add(new PlayerAnswer(questionId, optionId));
    }

    private boolean answerExist(int questionId) {
        return answers
            .stream()
            .anyMatch(answer -> questionId == answer.getQuestionId());
    }


}
