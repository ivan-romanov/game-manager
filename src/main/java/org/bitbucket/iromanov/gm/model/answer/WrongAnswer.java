package org.bitbucket.iromanov.gm.model.answer;

import static org.bitbucket.iromanov.gm.model.answer.AnswerVerificationStatus.WRONG;

import lombok.AllArgsConstructor;
import org.springframework.lang.NonNull;

@AllArgsConstructor
public class WrongAnswer implements AnswerVerificationResult {

    @Override
    @NonNull
    public AnswerVerificationStatus getStatus() {
        return WRONG;
    }

    @Override
    public int getPoints() {
        return 0;
    }
}
