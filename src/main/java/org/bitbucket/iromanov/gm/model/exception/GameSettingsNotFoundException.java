package org.bitbucket.iromanov.gm.model.exception;


public class GameSettingsNotFoundException extends RuntimeException {

    public GameSettingsNotFoundException(String message) {
        super(message);
    }
}
